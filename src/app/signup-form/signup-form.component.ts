import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { tap, catchError } from 'rxjs/operators';
import { ApiServiceService } from './../api-service.service';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent implements OnInit {
  username: any;
  password: any;
  c_password: any;
  auth_token: any;
  email: any;
  message = '';

  constructor(
    private apiService: ApiServiceService,
    private http: HttpClient,
    private router: Router) { }

  ngOnInit() {
  }

  signup() {
    const data = {
      'password' : this.password,
      'email' : this.email
    };
    this.apiService.postData('register', data)
    .subscribe(result => {
      console.log(result);
      if (result['success']) {
        this.router.navigate(['login']);
      } else {
        this.message = result['error'];
      }
    }, err => {
      // console.log(err);
      this.message = err.error.msg;
    });
  }

}
